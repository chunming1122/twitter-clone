This is the project about cloning twitter UI, using the Sanity CMS, Next.js, Tailwind CSS, Typescript.
The functions include creating and reading tweets, comments. Also the function of "Login with Twitter" with Next-Auth.


===For running this project===
1. open your terminal and make sure you are in the directory of "TWITTER-CLONE"
2. `yarn install`
3. `yarn run dev`